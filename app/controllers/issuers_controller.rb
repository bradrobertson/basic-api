class IssuersController < ApplicationController
  before_action :set_issuer, only: [:show, :update, :destroy]

  # GET /issuers
  def index
    @issuers = Issuer.all

    render json: @issuers
  end

  # GET /issuers/1
  def show
    render json: @issuer
  end

  # POST /issuers
  def create
    @issuer = Issuer.new(issuer_params)

    if @issuer.save
      render json: @issuer, status: :created, location: @issuer
    else
      render json: @issuer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /issuers/1
  def update
    if @issuer.update(issuer_params)
      render json: @issuer
    else
      render json: @issuer.errors, status: :unprocessable_entity
    end
  end

  # DELETE /issuers/1
  def destroy
    @issuer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_issuer
      @issuer = Issuer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def issuer_params
      params.require(:issuer).permit(:name)
    end
end
