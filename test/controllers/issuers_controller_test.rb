require 'test_helper'

class IssuersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @issuer = issuers(:one)
  end

  test "should get index" do
    get issuers_url, as: :json
    assert_response :success
  end

  test "should create issuer" do
    assert_difference('Issuer.count') do
      post issuers_url, params: { issuer: { name: @issuer.name } }, as: :json
    end

    assert_response 201
  end

  test "should show issuer" do
    get issuer_url(@issuer), as: :json
    assert_response :success
  end

  test "should update issuer" do
    patch issuer_url(@issuer), params: { issuer: { name: @issuer.name } }, as: :json
    assert_response 200
  end

  test "should destroy issuer" do
    assert_difference('Issuer.count', -1) do
      delete issuer_url(@issuer), as: :json
    end

    assert_response 204
  end
end
